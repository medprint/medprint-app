import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-new-request',
  templateUrl: './new-request.page.html',
  styleUrls: ['./new-request.page.scss'],
})
export class NewRequestPage implements OnInit {

  constructor() { }

  ngOnInit() {
  }
  AuftragAnnehmen() {
    alert('Danke für die Annahme des Auftrags!');
  }
  AuftragAblehnen() {
    alert('Schade, dass du diesen Auftrag abgelehnt hast!');
  }
}
