import { Component, OnInit } from '@angular/core';
import { Platform } from '@ionic/angular';
import { ApiDjangoService } from '../../services/api-django.service';
import { Router } from '@angular/router';
import CryptoJS from 'crypto-js';

@Component({
  selector: 'app-register-page',
  templateUrl: './register-page.page.html',
  styleUrls: ['./register-page.page.scss'],
})

export class RegisterPagePage implements OnInit {
  registerCredentials = { username: '', email: '', passwort: '', passwortbestaetigung: '' };
  constructor(
      public apiService: ApiDjangoService,
      public router: Router) {

  }

  ngOnInit() {
  }


  register() {

    if (this.registerCredentials.passwort !== this.registerCredentials.passwortbestaetigung) {
      this.apiService.showError('Passwortbestätigung nicht korrekt!');
    } else if (this.registerCredentials.username.length === 0 && this.registerCredentials.email.length == 0) {
      this.apiService.showError('Bitte gib Deinen Benutzernamen und Deine Email-Adresse ein!');
    } else {

      if (this.apiService.networkConnected) {
        this.apiService.showLoading();

        // Check email
        const queryPath = '?email=' + this.registerCredentials.email;
        this.apiService.findUser(queryPath).subscribe((listUser) => {
          this.apiService.stopLoading()
          console.log(JSON.stringify(listUser))
          if (listUser) {
            const nbUserFound = listUser['count']
            if (nbUserFound===0){
              const encryptedPassword = CryptoJS.SHA256(this.registerCredentials.passwort).toString(CryptoJS.enc.Hex);
              const userToCreate = {
                'email': this.registerCredentials.email,
                'username': this.registerCredentials.username,
                'password': encryptedPassword,
                'valid': true,
                'is_active': true,
                'is_staff': false
              }

              this.apiService.createUser(userToCreate).subscribe((resultat) => {
                if (resultat) {
                  this.router.navigateByUrl('/show-bikes');
                } else {
                  this.apiService.stopLoading();
                  this.apiService.showError('Achtung, während der Registrierung ist ein Fehler aufgetaucht!');
                }
              });
            } else{
              this.apiService.showError('Achtung, es gibt bereits einen Account mit dieser E-Mail-Adresse, bitte einloggen!');
            }

          } else {

            this.apiService.showError('Achtung, während der Registrierung ist ein Fehler aufgetaucht!' );

          }
        });

      }
    }
  }

}

