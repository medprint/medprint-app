import { Component, OnInit } from '@angular/core';
import {isNotNullOrUndefined} from 'codelyzer/util/isNotNullOrUndefined';
import {NavController} from '@ionic/angular';
import {OverviewPage} from './overview/overview.page';
import {StorageService} from '../storage.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.page.html',
  styleUrls: ['./home.page.scss'],
})
export class HomePage implements OnInit {
    constructor(private nav: NavController, public storageService: StorageService) {
    }

    mailadresse: string;
    plz: number;

    ngOnInit() {
        this.storageService.get('emailadresse').then(emailadresse => {
            this.mailadresse = emailadresse;
            console.log('emailadresse wurde gelesen');
        }).catch(e => {
            console.log('error: ' + e);
        });

        this.storageService.get('PLZ').then(PLZ => {
            this.plz = PLZ;
            console.log('PLZ wurde gelesen');
        }).catch(e => {
            console.log('error: ' + e);
        });

        if (this.mailadresse !== null && this.plz !== null) {
            console.log("Weiterleitung erfolgt");
            this.nav.navigateForward('/overview');
        }
    }

    login() {
        console.log(this.mailadresse + ' ' +  this.plz);
        if ((this.mailadresse === null) && (this.plz === null)) {
            alert('bitte email-Adresse und PLZ eintragen!');
        } else if (this.mailadresse === null) {
            alert('bitte email-Adresse eintragen!');
        } else if (this.plz === null) {
            alert('bitte PLZ eintragen!');
        } else {
            this.storageService.set('emailadresse:', this.mailadresse).then(result => {
                console.log('emailadresse wurde gespeichert');
            }).catch(e => {
                console.log('error ' + e);
            });
            this.storageService.set('PLZ:', this.plz).then(result => {
                console.log('PLZ wurde gespeichert');
            }).catch(e => {
                console.log('error ' + e);
            });
            this.nav.navigateForward('./home/overview');
        }
    }
}
